import os
import json

from PyQt5.QtCore import QSettings

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

CONF_PATH = os.path.join(BASE_DIR, 'price_engine.conf')

_settings = QSettings(CONF_PATH, QSettings.IniFormat)
app = None



def get(key, default=None):
    return _settings.value(key, default)

def get_json(query_param):
    result = None
    value = get(query_param)

    if value:
        try:
            result = json.loads(get(query_param))
        except json.JSONDecodeError as ex:
            #TODO: replace with yaml which supports multiline strings
            result = json.loads(get(query_param).replace('\n', ''))
    return result

def set(key, value):
    _settings.setValue(key, value)
