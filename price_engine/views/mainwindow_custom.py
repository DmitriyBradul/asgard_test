# -*- coding: utf-8 -*-
import sys

from PyQt5 import QtGui, QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QApplication, QListWidgetItem, QTableWidgetItem, QAction

from price_engine.settings import config
from price_engine.core.entities.revision import Revision
from price_engine.core.serializers import ModelSerializer
from price_engine.core.adapters.data_adapter import DataAdapter
from price_engine.core.data_types import DataField
from price_engine.core.ui_models.revision_model import RevisionModel
from price_engine.common import storage
from price_engine.views.preferences_custom import PreferencesDialog

import logging

_logger = logging.getLogger(__name__)


class Ui_MainWindow_Custom(object):
    def setupUi(self, Ui_MainWindow, Ui_MainWindow_Actions):
        self._window = Ui_MainWindow
        Ui_MainWindow._window = Ui_MainWindow
        self._window_actions = Ui_MainWindow_Actions

        self._window.load_mainwindow_metadata = self.load_mainwindow_metadata
        self._window.load_mainwindow_revisions = self.load_mainwindow_revisions

        self.setup_mainwindow_revisions_toolbutton()
        self.setup_mainwindow_revisions()
        self.setup_mainwindow_statusbar()
        self.setup_mainwindow_menu()

    def on_startup(self):
        """
        On startup actions
        :return:
        """
        self.load_mainwindow_metadata()
        self.load_mainwindow_revisions()

    def load_mainwindow_metadata(self):
        """

        :return:
        """
        try:
            QApplication.setOverrideCursor(Qt.WaitCursor)

            data_sources_metadata = config.get('metadata', {})

            def _update_tree(tree, types):
                data_sources_dict = {}
                iterator = QtWidgets.QTreeWidgetItemIterator(tree)
                while iterator.value():
                    item = iterator.value()
                    data_sources_dict[item.text(0)] = item
                    iterator += 1

                for data_source in data_sources_metadata:

                    if data_source in data_sources_dict:
                        tree_item = data_sources_dict[data_source]
                        for i in reversed(range(tree_item.childCount())):
                            tree_item.removeChild(tree_item.child(i))
                    else:
                        tree_item = QtWidgets.QTreeWidgetItem(tree)
                        tree_item.setText(0, data_source)

                    metadata = data_sources_metadata[data_source]
                    for sheet in metadata:
                        title = sheet['title']
                        sub_tree_item = QtWidgets.QTreeWidgetItem(tree_item)
                        sub_tree_item.setText(0, title)
                        sub_tree_item.setIcon(0, QtGui.QIcon('./resources/icons/blank-file.png'))

                        for i, header in enumerate(sheet['column_headers']):
                            if sheet['column_types'][i] in types:
                                sub_sub_tree_item = QtWidgets.QTreeWidgetItem(sub_tree_item)
                                sub_sub_tree_item.setText(0, header)
                                sub_sub_tree_item.setIcon(0, QtGui.QIcon(types[sheet['column_types'][i]]))
                                data_field = DataField(
                                    data_source=tree_item,
                                    table=sub_tree_item,
                                    column=sub_sub_tree_item,
                                    #                    type=float if ... else None
                                )
                                sub_sub_tree_item.setData(0, Qt.UserRole, data_field)


            _update_tree(self._window.dimentionsTree, DataAdapter.DIMENSION_TYPES)
            _update_tree(self._window.measuresTree, DataAdapter.MEASURE_TYPES)

            _logger.info('Successful reading metadata!')

        except Exception as ex:
            QtWidgets.QMessageBox.warning(None, 'Reading metadata', 'Error: %s' % ex)

        finally:
            QApplication.restoreOverrideCursor()

    def load_mainwindow_revisions(self):
        """

        :return:
        """
        records = storage.get('revision', filter={})
        if records.count():
            for record in records:
                _, revision, _ = ModelSerializer.unpack(record.get('value'))
                self._window.versionsList.model().appendRow(revision)

    def setup_mainwindow_revisions_toolbutton(self):
        """

        :return:
        """
        self._window.revisionsAddBtn.setPopupMode(QtWidgets.QToolButton.MenuButtonPopup)
        self._window.revisionsAddBtn.setMenu(QtWidgets.QMenu(self._window.revisionsAddBtn))

        action0 = QAction(text="Save rules+data",
                          parent=self._window.centralWidget,
                          triggered=self._window_actions.on_mainwindow_revisions_all_add_button_click)
        action1 = QAction(text="Save rules",
                          parent=self._window.centralWidget,
                          triggered=self._window_actions.on_mainwindow_revisions_rules_add_button_click)
        # action2 = QAction(text="Save data",
        #                   parent=self._window.centralWidget,
        #                   triggered=self._window_actions.on_mainwindow_revisions_data_add_button_click)

        action0.setIcon(QtGui.QIcon('./resources/icons/save-doc2.png'))
        action1.setIcon(QtGui.QIcon('./resources/icons/save-doc.png'))
        # action2.setIcon(QtGui.QIcon('./resources/icons/save-data.png'))

        self._window.revisionsAddBtn.menu().addAction(action0)
        self._window.revisionsAddBtn.menu().addAction(action1)
        # self._window.revisionsAddBtn.menu().addAction(action2)

    def setup_mainwindow_revisions(self):
        """

        :return:
        """
        tree_model = RevisionModel()
        self._window.versionsList.setModel(tree_model)
        self._window.versionsList.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self._window.versionsList.header().setSectionResizeMode(QtWidgets.QHeaderView.ResizeToContents)
        self._window.versionsList.setSortingEnabled(True)

    def setup_mainwindow_statusbar(self):
        """

        :return:
        """
        self._window.statusBar.showMessage("Ready")

    def setup_mainwindow_menu(self):
        """

        :return:
        """
        # self._window.actionExit.triggered = self._window._mainwindow.close
        self._window.actionExit.triggered.connect(self._window._mainwindow.close)
        self._window.actionAbout_Qt.triggered.connect(QApplication.instance().aboutQt)
        self._window.actionPreferences.triggered.connect(self.on_preferences_click)

    def on_preferences_click(self):
        prefs = PreferencesDialog()
        prefs.exec_()