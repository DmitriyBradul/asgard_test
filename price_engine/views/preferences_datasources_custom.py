# -*- coding: utf-8 -*-

from PyQt5.QtWidgets import QWidget

from . import preferences_datasources

import logging

_logger = logging.getLogger(__name__)


class DataSourcesPage(QWidget, preferences_datasources.Ui_dataSourcesPage):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setupUi(self)

    def setupUi(self, dialog):
        super().setupUi(dialog)
