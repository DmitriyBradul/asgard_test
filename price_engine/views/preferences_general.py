# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/dbhost/Work/qt_test/price_engine/price_engine/views/preferences_general.ui'
#
# Created by: PyQt5 UI code generator 5.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_generalPage(object):
    def setupUi(self, generalPage):
        generalPage.setObjectName("generalPage")
        generalPage.resize(405, 352)
        self.horizontalLayout = QtWidgets.QHBoxLayout(generalPage)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.checkBox = QtWidgets.QCheckBox(generalPage)
        self.checkBox.setObjectName("checkBox")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.checkBox)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.formLayout.setItem(2, QtWidgets.QFormLayout.SpanningRole, spacerItem)
        self.horizontalLayout.addLayout(self.formLayout)

        self.retranslateUi(generalPage)
        QtCore.QMetaObject.connectSlotsByName(generalPage)

    def retranslateUi(self, generalPage):
        _translate = QtCore.QCoreApplication.translate
        generalPage.setWindowTitle(_translate("generalPage", "Form"))
        self.checkBox.setText(_translate("generalPage", "Ask user on exit"))

