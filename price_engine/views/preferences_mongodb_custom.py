# -*- coding: utf-8 -*-
import logging

from PyQt5.QtWidgets import QWidget

from . import preferences_mongodb

_logger = logging.getLogger(__name__)


class MongoDbPage(QWidget, preferences_mongodb.Ui_mongoDbPage):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setupUi(self)

    def setupUi(self, dialog):
        super().setupUi(dialog)

        self.hostLineEdit.setText('192.168.88.248')
        self.portLineEdit.setText('27017')
        self.dBNameLineEdit.setText('main')
