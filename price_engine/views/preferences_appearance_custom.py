# -*- coding: utf-8 -*-

from PyQt5.QtWidgets import QWidget, QStyleFactory

from . import preferences_appearance
from price_engine.settings import config

import logging

_logger = logging.getLogger(__name__)


class AppearancePage(QWidget, preferences_appearance.Ui_appearancePage):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setupUi(self)

    def setupUi(self, dialog):
        super().setupUi(dialog)

        self.stylesComboBox.addItems(QStyleFactory.keys())
        self.stylesComboBox.setCurrentText('Fusion')
        self.stylesComboBox.currentTextChanged.connect(self.on_styles_combobox_changed)

    def on_styles_combobox_changed(self, text):
        if config.app:
            config.app.setStyle(QStyleFactory.create(text))