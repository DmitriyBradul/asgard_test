# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/dbhost/Work/qt_test/price_engine/price_engine/views/preferences.ui'
#
# Created by: PyQt5 UI code generator 5.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(819, 542)
        self.verticalLayout = QtWidgets.QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.treePreferences = QtWidgets.QTreeWidget(Dialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.treePreferences.sizePolicy().hasHeightForWidth())
        self.treePreferences.setSizePolicy(sizePolicy)
        self.treePreferences.setMaximumSize(QtCore.QSize(220, 16777215))
        self.treePreferences.setObjectName("treePreferences")
        item_0 = QtWidgets.QTreeWidgetItem(self.treePreferences)
        item_1 = QtWidgets.QTreeWidgetItem(item_0)
        item_0 = QtWidgets.QTreeWidgetItem(self.treePreferences)
        item_0 = QtWidgets.QTreeWidgetItem(self.treePreferences)
        self.horizontalLayout.addWidget(self.treePreferences)
        self.pagesWidget = QtWidgets.QStackedWidget(Dialog)
        self.pagesWidget.setObjectName("pagesWidget")
        self.horizontalLayout.addWidget(self.pagesWidget)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(Dialog)
        self.buttonBox.accepted.connect(Dialog.accept)
        self.buttonBox.rejected.connect(Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Preferences"))
        self.treePreferences.headerItem().setText(0, _translate("Dialog", "/"))
        __sortingEnabled = self.treePreferences.isSortingEnabled()
        self.treePreferences.setSortingEnabled(False)
        self.treePreferences.topLevelItem(0).setText(0, _translate("Dialog", "General"))
        self.treePreferences.topLevelItem(0).child(0).setText(0, _translate("Dialog", "Appearance"))
        self.treePreferences.topLevelItem(1).setText(0, _translate("Dialog", "Data sources"))
        self.treePreferences.topLevelItem(2).setText(0, _translate("Dialog", "MongoDB"))
        self.treePreferences.setSortingEnabled(__sortingEnabled)

