# -*- coding: utf-8 -*-
import datetime

from collections import namedtuple

from PyQt5 import QtGui, QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QApplication, QListWidgetItem

from price_engine.core.factory import AbstractFactory
from price_engine.core.data_types import DataFilter, DataField, DataExpression
from price_engine.core.entities.revision import Revision
from price_engine.core.serializers import ModelSerializer, RuleRevisionSerializer, DataRevisionSerializer,\
    RuleDataRevisionSerializer
from price_engine.settings import config
from price_engine.common import storage
from price_engine.views.source_code_edit import Ui_sourceCodeEditDialog
from price_engine.views.source_code_edit_custom import SourceCodeEditDialog
from price_engine.common import syntax_highlighter

import logging

_logger = logging.getLogger(__name__)


DataTuple = namedtuple('DataTuple', ['data_source', 'table', 'columns'])
VAR_COUNT = 1


class Ui_MainWindow_Actions(object):
    """
    Connect UI widget slots with handlers
    """
    def __init__(self, ui_mainwindow):
        self._window = ui_mainwindow


    def connect_handlers(self):
        self._window.readMetadataButton.clicked.connect(self.on_mainwindow_metadata_button_click)
        self._window.dataSources.itemDoubleClicked.connect(self.on_mainwindow_metadata_sources_doubleclick)
        self._window.dimensionsDelBtn.clicked.connect(self.on_mainwindow_dimentions_del_button_click)
        self._window.measuresDelBtn.clicked.connect(self.on_mainwindow_measures_del_button_click)
        self._window.columnsDelBtn.clicked.connect(self.on_mainwindow_columns_del_button_click)
        self._window.columnsAddBtn.clicked.connect(self.on_mainwindow_columns_add_button_click)
        self._window.columnsEditBtn.clicked.connect(self.on_mainwindow_columns_edit_button_click)
        self._window.columnsList.itemDoubleClicked.connect(self.on_mainwindow_columns_doubleclick)
        self._window.revisionsAddBtn.clicked.connect(self.on_mainwindow_revisions_all_add_button_click)
        self._window.revisionsDelBtn.clicked.connect(self.on_mainwindow_revisions_del_button_click)
        self._window.revisionsEditBtn.clicked.connect(self.on_mainwindow_revisions_edit_button_click)
        self._window.versionsList.doubleClicked.connect(self.on_mainwindow_revisions_list_doubleclick)
        self._window.dimentionsTree.itemDoubleClicked.connect(lambda source=self._window.dimentionsTree:
                                                              self.on_mainwindow_tree_doubleclick(source))
        self._window.measuresTree.itemDoubleClicked.connect(lambda source=self._window.measuresTree:
                                                            self.on_mainwindow_tree_doubleclick(source))
        self._window.readDataBtn.clicked.connect(self.on_mainwindow_read_data_button_click)


    def on_mainwindow_metadata_sources_doubleclick(self, source):
        data_source_name = source.text()
        uri = config.get(data_source_name)
        dialog = SourceCodeEditDialog()
        dialog.set_code(uri)
        dialog.set_title('%s Data Source URI:' % data_source_name)
        dialog.setWindowTitle('Data Source')
        dialog.show()
        resp = dialog.exec_()

        if resp == QtWidgets.QDialog.Accepted:
            data_source_value = dialog.code()
            _logger.info('%s' % data_source_value)
            config.set(data_source_name, data_source_value)
            _logger.info('Data source updated: \'%s\'' % data_source_name)
        else:
            _logger.info('Cancelled')


    def on_mainwindow_revisions_list_doubleclick(self):
        """

        :return:
        """
        for index in self._window.versionsList.selectedIndexes():
            revision = self._window.versionsList.model().itemData(index)
            if revision:
                records = storage.get('revision', {'hash': revision.hash})
                if records.count():
                    serializer, obj, values = ModelSerializer.unpack(records[0].get('value'))
                    serializer.deserialize(self._window, values)


    def on_mainwindow_revisions_del_button_click(self):
        """

        :return:
        """
        for index in self._window.versionsList.selectedIndexes():
            revision = self._window.versionsList.model().itemData(index)
            if revision:
                storage.remove('revision', {'hash': revision.hash})
                self._window.versionsList.model().removeRow(index.row())


    def on_mainwindow_revisions_edit_button_click(self):
        """

        :return:
        """
        for index in self._window.versionsList.selectedIndexes():
            revision = self._window.versionsList.model().itemData(index)
            if revision:
        # for selected_item in self._window.versionsList.selectedItems():
        #         revision = selected_item.data(Qt.UserRole)
                text, ok = QtWidgets.QInputDialog.getText(
                    self._window.centralWidget,
                    'Text Input Dialog',
                    'Enter data revision description:',
                    text=revision.name
                )
                if ok:
                    hash = revision.hash
                    revision.name = text
                    #selected_item.setText(str(revision))
                    self._window.versionsList.model().setData(index, revision, Qt.UserRole)
                    #self._window.versionsList.model().appendRow(revision)
                    records = storage.get('revision', {'hash': hash})
                    if records.count():
                        _1, _2, _3 = ModelSerializer.unpack(records[0].get('value'))
                        new_value = ModelSerializer.pack(_1, revision, _3)
                        storage.update(
                            group='revision',
                            filter={'hash': hash},
                            new_object={'value': new_value, 'hash': revision.hash}
                        )


    def on_mainwindow_revisions_data_add_button_click(self):
        """

        :return:
        """
        text, ok = QtWidgets.QInputDialog.getText(
            self._window.centralWidget,
            'Text Input Dialog',
            'Enter data revision description:',
            text='Description ...'
        )
        if ok:
            revision = Revision(
                name=text,
                tag='D'
            )
            self._window.versionsList.model().appendRow(revision)
            serializer = DataRevisionSerializer()
            serialized = serializer.serialize(revision, self._window)
            storage.set('revision', {'hash': revision.hash,
                                     'value': serialized})


    def on_mainwindow_revisions_rules_add_button_click(self):
        """

        :return:
        """
        text, ok = QtWidgets.QInputDialog.getText(
            self._window.centralWidget,
            'Text Input Dialog',
            'Enter rules revision description:',
            text='Description ...'
        )
        if ok:
            revision = Revision(
                name=text,
                tag='R',
            )
            self._window.versionsList.model().appendRow(revision)
            serializer = RuleRevisionSerializer()
            serialized = serializer.serialize(revision, self._window)
            storage.set('revision', {'hash': revision.hash,
                                     'value': serialized})


    def on_mainwindow_revisions_all_add_button_click(self):
        """

        :return:
        """
        text, ok = QtWidgets.QInputDialog.getText(
            self._window.centralWidget,
            'Text Input Dialog',
            'Enter revision description:',
            text='Description ...'
        )
        if ok:
            revision = Revision(
                name=text,
                tag='D+R',
            )
            self._window.versionsList.model().appendRow(revision)
            serializer = RuleDataRevisionSerializer()
            serialized = serializer.serialize(revision, self._window)
            storage.set('revision', {'hash': revision.hash,
                                     'value': serialized})


    def on_mainwindow_metadata_button_click(self):
        """

        :return:
        """
        try:
            QApplication.setOverrideCursor(Qt.WaitCursor)
            data_sources_metadata = {}
            for i in range(self._window.dataSources.count()):
                data_source_name = str(self._window.dataSources.item(i).text())
                data_source_uri = config.get_json(data_source_name)
                if data_source_uri:
                    factory = AbstractFactory.get_factory(data_source_uri.get('type'))
                    data_fetcher = factory.fetcher(data_source_uri)
                    data_adapter = factory.adapter()
                    metadata = data_fetcher.fetch_metadata()
                    data_sources_metadata[data_source_name] = data_adapter.apply_metadata_schema(metadata)

            config.set('metadata', data_sources_metadata)

            self._window.load_mainwindow_metadata()

        except Exception as ex:
            QtWidgets.QMessageBox.warning(None, 'Reading metadata', 'Error: %s' % ex)

        finally:
            QApplication.restoreOverrideCursor()


    def on_mainwindow_dimentions_del_button_click(self):
        root = self._window.dimentionsTree.invisibleRootItem()
        for item in self._window.dimentionsTree.selectedItems():
            (item.parent() or root).removeChild(item)


    def on_mainwindow_measures_del_button_click(self):
        root = self._window.measuresTree.invisibleRootItem()
        for item in self._window.measuresTree.selectedItems():
            (item.parent() or root).removeChild(item)


    def on_mainwindow_columns_del_button_click(self):
        for selected_item in self._window.columnsList.selectedItems():
            self._window.columnsList.takeItem(self._window.columnsList.row(selected_item))


    def on_mainwindow_columns_add_button_click(self):
        global VAR_COUNT
        var = DataExpression(
            name='var_%d' % VAR_COUNT,
            expression='42'
        )

        item = QListWidgetItem(str(var), self._window.columnsList)
        item.setData(Qt.UserRole, var)
        VAR_COUNT += 1


    def on_mainwindow_columns_edit_button_click(self):
        pass
        # for selected_item in self._window.columnsList.selectedItems():
        #     rule = selected_item.text()
        #     text, ok = QtWidgets.QInputDialog.getText(
        #         self._window.centralWidget,
        #         'Text Input Dialog',
        #         'Enter data revision description:',
        #         text=rule
        #     )
        #     if ok:
        #         selected_item.setText(text)


    def on_mainwindow_columns_doubleclick(self, source):
        item_name = source.text()
        item_data = source.data(Qt.UserRole)

        dialog = SourceCodeEditDialog()
        dialog.set_code(item_data.expression)
        dialog.set_title('\'%s\' value:' % item_name)
        dialog.setWindowTitle('Column/Expression')
        dialog.show()
        resp = dialog.exec_()


        if resp == QtWidgets.QDialog.Accepted:
            item_data.expression = dialog.code()
            _logger.info('Column \'%s\' updated: %s' % (item_name, item_data.expression))
        else:
            _logger.info('Cancelled')


    def on_mainwindow_tree_doubleclick(self, source):
        EMPTY = '(empty)'
        if self._window.columnsList.count() == 1 and \
                self._window.columnsList.item(0).text() == EMPTY:
            self._window.columnsList.clear()
        selected_item = source
        is_leaf = selected_item.childCount() == 0
        if is_leaf:
            selected_item_str = selected_item.text(0)
            columns_list = [str(self._window.columnsList.item(i).text())
                            for i in range(self._window.columnsList.count())]
            if selected_item_str != EMPTY and selected_item_str not in columns_list:
                _1st, _2nd, _3rd = (selected_item.parent().parent().text(0),
                                    selected_item.parent().text(0),
                                    selected_item_str)
                fully_qualified_column = DataField(
                    data_source=_1st,
                    table=_2nd,
                    column=_3rd,
#                    type=float if ... else None
                )
                item = QListWidgetItem(str(fully_qualified_column), self._window.columnsList)
                item.setData(Qt.UserRole, fully_qualified_column)


    def on_mainwindow_read_data_button_click(self):
        QApplication.setOverrideCursor(Qt.WaitCursor)

        fields = []
        data_sets = []
        data_fields = [self._window.columnsList.item(i).data(Qt.UserRole)
                        for i in range(self._window.columnsList.count())
                        if isinstance(self._window.columnsList.item(i).data(Qt.UserRole), DataField)]

        data_expressions = [self._window.columnsList.item(i).data(Qt.UserRole)
                              for i in range(self._window.columnsList.count())
                              if isinstance(self._window.columnsList.item(i).data(Qt.UserRole), DataExpression)]

        columns_num = len(data_fields) + len(data_expressions)
        columns_names = [c.column for c in data_fields] + [c.name for c in data_expressions]

        if data_fields:
            data_sources, tables, column_name = [set(x) for x in zip(*data_fields)]
            for data_source in data_sources:
                field = DataFilter(data_source)
                for table in tables:
                    field.table = table
                    field.columns = [t.column for t in data_fields
                                      if t.data_source == data_source and t.table == table]
                fields.append(field)

        if fields:
            for field in fields:
                data_source_uri = config.get_json(field.name)
                factory = AbstractFactory.get_factory(data_source_uri.get('type'))
                data_fetcher = factory.fetcher(data_source_uri)
                data_set = data_fetcher.fetch_data(field)
                if data_set:
                    data_sets.append(data_set)

        # QApplication.restoreOverrideCursor()

        if data_sets:
            for data_set in data_sets:
                try:
                    self._window.dataTable.setRowCount(0)
                    self._window.dataTable.setColumnCount(0)

                    self._window.dataTable.setColumnCount(columns_num + 1)
                    self._window.dataTable.setHorizontalHeaderLabels(columns_names + ['_context'])
                    self._window.dataTable.setColumnHidden(columns_num, True)

                    for line in data_set.data:
                        rowPosition = self._window.dataTable.rowCount()
                        self._window.dataTable.insertRow(rowPosition)
                        row_context = {}
                        for j, column in enumerate(data_fields):
                            value = line[j]
                            if column.type == float:
                                value = (value or 0.0)
                            row_context.update({column.eval_expression : value})
                            self._window.dataTable.setItem(rowPosition, j, QtWidgets.QTableWidgetItem('%s' % value))

                        # set context
                        context_item = QtWidgets.QTableWidgetItem()
                        context_item.setData(Qt.UserRole, row_context)
                        context_item.setText(str(row_context))
                        self._window.dataTable.setItem(rowPosition, columns_num, context_item)

                except Exception as ex:
                    QtWidgets.QMessageBox.warning(None, 'Reading data', 'Error: %s' % ex)

        if data_expressions:
            for i in range(self._window.dataTable.rowCount()):
                max_num_tries = len(data_expressions) * len(data_expressions)
                success = False
                last_error = ''
                for _ in range(max_num_tries):
                    try:
                        for j, cell in enumerate(data_expressions):
                            row_context = self._window.dataTable.item(i, columns_num).data(Qt.UserRole)
                            cell.context = row_context.copy()
                            cell_value = cell.value()
                            cell.context = None
                            item = QtWidgets.QTableWidgetItem('%s' % cell_value)
                            row_context.update(
                                {cell.name: cell_value}
                            )
                            self._window.dataTable.item(i, columns_num).setData(Qt.UserRole, row_context)
                            self._window.dataTable.item(i, columns_num).setText(str(row_context))
                            self._window.dataTable.setItem(i, len(data_fields) + j, item)
                        success = True
                        break
                    except NameError as ex:
                        _logger.error('Exception: %s' % ex)
                        last_error = '%s' % ex

                    except Exception as ex:
                        _logger.error('Exception: %s' % ex)
                        break

                if not success:
                    msg = QtWidgets.QMessageBox()
                    #msg.setIcon(QtIcon.Critical)
                    msg.setText("Name resolving error:")
                    msg.setInformativeText(last_error)
                    msg.setWindowTitle("Error")
                    msg.exec_()
                    break

        QApplication.restoreOverrideCursor()