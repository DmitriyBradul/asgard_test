# -*- coding: utf-8 -*-

from PyQt5 import QtGui, QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QApplication, QListWidgetItem, QTableWidgetItem, QAction, QDialog

from . import preferences
from .preferences_mongodb_custom import MongoDbPage
from .preferences_appearance_custom import AppearancePage
from .preferences_general_custom import GeneralPage
from .preferences_datasources_custom import DataSourcesPage


import logging

_logger = logging.getLogger(__name__)


class PreferencesDialog(QDialog, preferences.Ui_Dialog):

    PAGE_MAP = {
        'General' : 0,
        'Appearance' : 1,
        'Data sources' : 2,
        'MongoDB' : 3
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setupUi(self)

    def setupUi(self, dialog):
        super().setupUi(dialog)

        self.pagesWidget.addWidget(GeneralPage())
        self.pagesWidget.addWidget(AppearancePage())
        self.pagesWidget.addWidget(DataSourcesPage())
        self.pagesWidget.addWidget(MongoDbPage())

        self.treePreferences.itemClicked.connect(self.on_preferences_tree_click)


    def on_preferences_tree_click(self, source):
        self.pagesWidget.setCurrentIndex(self.PAGE_MAP.get(source.text(0)))
