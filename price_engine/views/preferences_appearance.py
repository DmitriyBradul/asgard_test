# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/dbhost/Work/qt_test/price_engine/price_engine/views/preferences_appearance.ui'
#
# Created by: PyQt5 UI code generator 5.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_appearancePage(object):
    def setupUi(self, appearancePage):
        appearancePage.setObjectName("appearancePage")
        appearancePage.resize(405, 352)
        self.horizontalLayout = QtWidgets.QHBoxLayout(appearancePage)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.hostLabel = QtWidgets.QLabel(appearancePage)
        self.hostLabel.setObjectName("hostLabel")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.hostLabel)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.formLayout.setItem(1, QtWidgets.QFormLayout.SpanningRole, spacerItem)
        self.stylesComboBox = QtWidgets.QComboBox(appearancePage)
        self.stylesComboBox.setObjectName("stylesComboBox")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.stylesComboBox)
        self.horizontalLayout.addLayout(self.formLayout)

        self.retranslateUi(appearancePage)
        QtCore.QMetaObject.connectSlotsByName(appearancePage)

    def retranslateUi(self, appearancePage):
        _translate = QtCore.QCoreApplication.translate
        appearancePage.setWindowTitle(_translate("appearancePage", "Form"))
        self.hostLabel.setText(_translate("appearancePage", "Style"))

