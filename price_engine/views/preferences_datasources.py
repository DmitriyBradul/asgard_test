# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/dbhost/Work/qt_test/price_engine/price_engine/views/preferences_datasources.ui'
#
# Created by: PyQt5 UI code generator 5.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_dataSourcesPage(object):
    def setupUi(self, dataSourcesPage):
        dataSourcesPage.setObjectName("dataSourcesPage")
        dataSourcesPage.resize(405, 352)
        self.horizontalLayout = QtWidgets.QHBoxLayout(dataSourcesPage)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(dataSourcesPage)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.treeWidget = QtWidgets.QTreeWidget(dataSourcesPage)
        self.treeWidget.setWordWrap(True)
        self.treeWidget.setObjectName("treeWidget")
        item_0 = QtWidgets.QTreeWidgetItem(self.treeWidget)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        item_0.setFont(0, font)
        item_1 = QtWidgets.QTreeWidgetItem(item_0)
        font = QtGui.QFont()
        font.setItalic(True)
        item_1.setFont(1, font)
        item_1.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsEditable|QtCore.Qt.ItemIsDragEnabled|QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)
        item_1 = QtWidgets.QTreeWidgetItem(item_0)
        font = QtGui.QFont()
        font.setItalic(True)
        item_1.setFont(1, font)
        item_1.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsEditable|QtCore.Qt.ItemIsDragEnabled|QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)
        item_1 = QtWidgets.QTreeWidgetItem(item_0)
        font = QtGui.QFont()
        font.setItalic(True)
        item_1.setFont(1, font)
        item_1.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsEditable|QtCore.Qt.ItemIsDragEnabled|QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)
        item_1 = QtWidgets.QTreeWidgetItem(item_0)
        font = QtGui.QFont()
        font.setItalic(True)
        item_1.setFont(1, font)
        item_1.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsEditable|QtCore.Qt.ItemIsDragEnabled|QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)
        item_1 = QtWidgets.QTreeWidgetItem(item_0)
        font = QtGui.QFont()
        font.setItalic(True)
        item_1.setFont(1, font)
        item_1.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsEditable|QtCore.Qt.ItemIsDragEnabled|QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)
        item_1 = QtWidgets.QTreeWidgetItem(item_0)
        font = QtGui.QFont()
        font.setItalic(True)
        item_1.setFont(1, font)
        item_1.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsEditable|QtCore.Qt.ItemIsDragEnabled|QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)
        item_1 = QtWidgets.QTreeWidgetItem(item_0)
        font = QtGui.QFont()
        font.setItalic(True)
        item_1.setFont(1, font)
        item_1.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsEditable|QtCore.Qt.ItemIsDragEnabled|QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)
        item_0 = QtWidgets.QTreeWidgetItem(self.treeWidget)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        item_0.setFont(0, font)
        item_1 = QtWidgets.QTreeWidgetItem(item_0)
        font = QtGui.QFont()
        font.setItalic(True)
        item_1.setFont(1, font)
        item_1.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsEditable|QtCore.Qt.ItemIsDragEnabled|QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)
        item_1 = QtWidgets.QTreeWidgetItem(item_0)
        font = QtGui.QFont()
        font.setItalic(True)
        item_1.setFont(1, font)
        item_1.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsEditable|QtCore.Qt.ItemIsDragEnabled|QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)
        self.verticalLayout.addWidget(self.treeWidget)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.horizontalLayout.addLayout(self.verticalLayout)

        self.retranslateUi(dataSourcesPage)
        QtCore.QMetaObject.connectSlotsByName(dataSourcesPage)

    def retranslateUi(self, dataSourcesPage):
        _translate = QtCore.QCoreApplication.translate
        dataSourcesPage.setWindowTitle(_translate("dataSourcesPage", "Form"))
        self.label.setText(_translate("dataSourcesPage", "Data sources:"))
        self.treeWidget.headerItem().setText(0, _translate("dataSourcesPage", "name"))
        self.treeWidget.headerItem().setText(1, _translate("dataSourcesPage", "key"))
        self.treeWidget.headerItem().setText(2, _translate("dataSourcesPage", "value"))
        __sortingEnabled = self.treeWidget.isSortingEnabled()
        self.treeWidget.setSortingEnabled(False)
        self.treeWidget.topLevelItem(0).setText(0, _translate("dataSourcesPage", "Mapper"))
        self.treeWidget.topLevelItem(0).child(0).setText(1, _translate("dataSourcesPage", "type"))
        self.treeWidget.topLevelItem(0).child(0).setText(2, _translate("dataSourcesPage", "postgresql"))
        self.treeWidget.topLevelItem(0).child(1).setText(1, _translate("dataSourcesPage", "uri"))
        self.treeWidget.topLevelItem(0).child(1).setText(2, _translate("dataSourcesPage", "        SELECT\n"
"            keywords.Category as Category,\n"
"            keywords.Keywords as Keywords,\n"
"            result.Brand as Brand,\n"
"            result.ASIN as ASIN,\n"
"            result.Searched_On as Searched_On,\n"
"            result.Landed_Price as Landed_Price,\n"
"            ident.Site as Site,\n"
"            ident.Category as \\\"Category (Extract Product Identifier)\\\",\n"
"            ident.Value as Value,\n"
"            ident.Product as Product,\n"
"            product.External_ID as External_ID\n"
"        FROM (\n"
"            SELECT\n"
"                    Category,\n"
"                    Keywords\n"
"            FROM\n"
"                    search_keywords) keywords\n"
"\n"
"        LEFT OUTER JOIN (\n"
"            SELECT\n"
"                    Brand,\n"
"                    ASIN,\n"
"                    Searched_On,\n"
"                    Landed_Price,\n"
"                    Keywords\n"
"            FROM\n"
"                    search_keyword_result\n"
"            WHERE\n"
"                    Searched_On >= current_date - integer \'300\') result\n"
"        ON (keywords.Keywords = result.Keywords)\n"
"\n"
"        LEFT OUTER JOIN (\n"
"            SELECT\n"
"                    Site,\n"
"                    Category,\n"
"                    Value,\n"
"                    Product\n"
"            FROM\n"
"                    extract_product_identifier) ident\n"
"        ON (ident.value = result.ASIN)\n"
"\n"
"        LEFT OUTER JOIN (\n"
"            SELECT\n"
"                    ID,\n"
"                    External_ID\n"
"            FROM\n"
"                    extract_product) product\n"
"        ON (ident.Product = product.ID)\n"
"    ORDER BY keywords\n"
"    LIMIT 2000\n"
""))
        self.treeWidget.topLevelItem(0).child(2).setText(1, _translate("dataSourcesPage", "host"))
        self.treeWidget.topLevelItem(0).child(2).setText(2, _translate("dataSourcesPage", "192.168.88.248"))
        self.treeWidget.topLevelItem(0).child(3).setText(1, _translate("dataSourcesPage", "port"))
        self.treeWidget.topLevelItem(0).child(3).setText(2, _translate("dataSourcesPage", "3456"))
        self.treeWidget.topLevelItem(0).child(4).setText(1, _translate("dataSourcesPage", "dbname"))
        self.treeWidget.topLevelItem(0).child(4).setText(2, _translate("dataSourcesPage", "openerp"))
        self.treeWidget.topLevelItem(0).child(5).setText(1, _translate("dataSourcesPage", "username"))
        self.treeWidget.topLevelItem(0).child(5).setText(2, _translate("dataSourcesPage", "openerp"))
        self.treeWidget.topLevelItem(0).child(6).setText(1, _translate("dataSourcesPage", "password"))
        self.treeWidget.topLevelItem(0).child(6).setText(2, _translate("dataSourcesPage", "********"))
        self.treeWidget.topLevelItem(1).setText(0, _translate("dataSourcesPage", "Google_Sheet"))
        self.treeWidget.topLevelItem(1).child(0).setText(1, _translate("dataSourcesPage", "type"))
        self.treeWidget.topLevelItem(1).child(0).setText(2, _translate("dataSourcesPage", "spreadsheets"))
        self.treeWidget.topLevelItem(1).child(1).setText(1, _translate("dataSourcesPage", "uri"))
        self.treeWidget.topLevelItem(1).child(1).setText(2, _translate("dataSourcesPage", "https://docs.google.com/spreadsheets/d/1JFS9_Mhd538dr1NQh9ZFP1xarDxido7KM5-UNdRfCes/edit?ts=5db058f6#gid=0"))
        self.treeWidget.setSortingEnabled(__sortingEnabled)

