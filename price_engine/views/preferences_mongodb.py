# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/dbhost/Work/qt_test/price_engine/price_engine/views/preferences_mongodb.ui'
#
# Created by: PyQt5 UI code generator 5.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_mongoDbPage(object):
    def setupUi(self, mongoDbPage):
        mongoDbPage.setObjectName("mongoDbPage")
        mongoDbPage.resize(405, 352)
        self.horizontalLayout = QtWidgets.QHBoxLayout(mongoDbPage)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.hostLabel = QtWidgets.QLabel(mongoDbPage)
        self.hostLabel.setObjectName("hostLabel")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.hostLabel)
        self.hostLineEdit = QtWidgets.QLineEdit(mongoDbPage)
        self.hostLineEdit.setObjectName("hostLineEdit")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.hostLineEdit)
        self.portLabel = QtWidgets.QLabel(mongoDbPage)
        self.portLabel.setObjectName("portLabel")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.portLabel)
        self.portLineEdit = QtWidgets.QLineEdit(mongoDbPage)
        self.portLineEdit.setObjectName("portLineEdit")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.portLineEdit)
        self.dBNameLabel = QtWidgets.QLabel(mongoDbPage)
        self.dBNameLabel.setObjectName("dBNameLabel")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.dBNameLabel)
        self.dBNameLineEdit = QtWidgets.QLineEdit(mongoDbPage)
        self.dBNameLineEdit.setText("")
        self.dBNameLineEdit.setObjectName("dBNameLineEdit")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.dBNameLineEdit)
        self.usernameLabel = QtWidgets.QLabel(mongoDbPage)
        self.usernameLabel.setObjectName("usernameLabel")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.usernameLabel)
        self.usernameLineEdit = QtWidgets.QLineEdit(mongoDbPage)
        self.usernameLineEdit.setObjectName("usernameLineEdit")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.usernameLineEdit)
        self.passwordLabel = QtWidgets.QLabel(mongoDbPage)
        self.passwordLabel.setObjectName("passwordLabel")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.passwordLabel)
        self.passwordLineEdit = QtWidgets.QLineEdit(mongoDbPage)
        self.passwordLineEdit.setObjectName("passwordLineEdit")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.passwordLineEdit)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.formLayout.setItem(5, QtWidgets.QFormLayout.SpanningRole, spacerItem)
        self.horizontalLayout.addLayout(self.formLayout)

        self.retranslateUi(mongoDbPage)
        QtCore.QMetaObject.connectSlotsByName(mongoDbPage)

    def retranslateUi(self, mongoDbPage):
        _translate = QtCore.QCoreApplication.translate
        mongoDbPage.setWindowTitle(_translate("mongoDbPage", "Form"))
        self.hostLabel.setText(_translate("mongoDbPage", "Host"))
        self.portLabel.setText(_translate("mongoDbPage", "Port"))
        self.dBNameLabel.setText(_translate("mongoDbPage", "DB name"))
        self.usernameLabel.setText(_translate("mongoDbPage", "Username"))
        self.passwordLabel.setText(_translate("mongoDbPage", "Password"))

