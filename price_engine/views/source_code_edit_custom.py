# -*- coding: utf-8 -*-
import logging

from PyQt5.QtWidgets import QDialog

from . import source_code_edit
from price_engine.common import syntax_highlighter

_logger = logging.getLogger(__name__)


class SourceCodeEditDialog(QDialog, source_code_edit.Ui_sourceCodeEditDialog):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setupUi(self)

    def setupUi(self, dialog):
        super().setupUi(dialog)
        #self.dialog_text.cursorPositionChanged.connect(self.on_cursor_changed)

    def on_cursor_changed(self):
        _ = syntax_highlighter.SqlHighlighter(self.dialog_text.document())

    def set_code(self, code):
        _ = syntax_highlighter.SqlHighlighter(self.dialog_text.document())
        self.dialog_text.setText(code)

    def set_title(self, title):
        self.dialog_label.setText(title)

    def code(self):
        return self.dialog_text.toPlainText()