# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/dbhost/Work/qt_test/price_engine/price_engine/views/source_code_edit.ui'
#
# Created by: PyQt5 UI code generator 5.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_sourceCodeEditDialog(object):
    def setupUi(self, sourceCodeEditDialog):
        sourceCodeEditDialog.setObjectName("sourceCodeEditDialog")
        sourceCodeEditDialog.setWindowModality(QtCore.Qt.NonModal)
        sourceCodeEditDialog.resize(643, 213)
        sourceCodeEditDialog.setModal(True)
        self.verticalLayout = QtWidgets.QVBoxLayout(sourceCodeEditDialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.dialog_label = QtWidgets.QLabel(sourceCodeEditDialog)
        self.dialog_label.setObjectName("dialog_label")
        self.verticalLayout.addWidget(self.dialog_label)
        self.dialog_text = QtWidgets.QTextEdit(sourceCodeEditDialog)
        self.dialog_text.setObjectName("dialog_text")
        self.verticalLayout.addWidget(self.dialog_text)
        self.buttonBox = QtWidgets.QDialogButtonBox(sourceCodeEditDialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(sourceCodeEditDialog)
        self.buttonBox.accepted.connect(sourceCodeEditDialog.accept)
        self.buttonBox.rejected.connect(sourceCodeEditDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(sourceCodeEditDialog)

    def retranslateUi(self, sourceCodeEditDialog):
        _translate = QtCore.QCoreApplication.translate
        sourceCodeEditDialog.setWindowTitle(_translate("sourceCodeEditDialog", "Dialog"))
        self.dialog_label.setText(_translate("sourceCodeEditDialog", "Source Code:"))

