# -*- coding: utf-8 -*-
import logging

from PyQt5.QtWidgets import QWidget

from . import preferences_general


_logger = logging.getLogger(__name__)


class GeneralPage(QWidget, preferences_general.Ui_generalPage):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setupUi(self)

    def setupUi(self, dialog):
        super().setupUi(dialog)
