import psycopg2
import psycopg2.extras

from .data_provider import DataProvider
from price_engine.core.data_types import DataSet, DataFilter


class PosgresqlDataProvider(DataProvider):

    METADATA_QUERY = "SELECT oid, typname FROM pg_type ORDER BY oid"
    TYPES_CONVERT = {
        'varchar' : 'STRING',
        'timestamp': 'DATETIME',
        'float8': 'NUMBER',
        'int4': 'NUMBER',
        'bool': 'BOOLEAN'
    }

    def __init__(self, uri_config):
        super().__init__(uri_config)
        self._client = None
        self._metadata = {}


    def connect(self):
        conn = psycopg2.connect(
            database=self._uri_config.get('dbname'),
            user=self._uri_config.get('username'),
            host=self._uri_config.get('host'),
            password=self._uri_config.get('password'),
        )

        cur = conn.cursor()
        cur.execute('SELECT version()')

        self._client = conn


    def _fetch_metadata(self):

        cur = None
        try:
            cur = self._client.cursor(cursor_factory=psycopg2.extras.DictCursor)
            cur.execute(self.METADATA_QUERY)  # self._uri_config.get('uri'))
            type_map = {rec['oid']:rec['typname'] for rec in cur.fetchall()}

            query = self._uri_config.get('uri')
            if query:
                query = "SELECT * FROM (%s) as t  LIMIT 10" % (self._uri_config.get('uri'))
                # data_query += ' LIMIT 10'
            cur.execute(query)

            tag = 'default'
            field_names = [col.name for col in cur.description]
            field_types = [type_map[col.type_code] for col in cur.description]

            self._metadata = (tag, field_names, field_types)

        finally:
            if cur:
                cur.close()


    def fetch_metadata(self):
        if not self._client:
            self.connect()

        if not self._metadata:
            self._fetch_metadata()

        return self._metadata


    def fetch_data(self, filter):
        self.fetch_metadata()
        cur = None
        query = self._uri_config.get('uri')

        if filter.columns:
            query = "SELECT %s from (%s) as t" % (', '.join(filter.columns),
                                                  self._uri_config.get('uri'))

        try:
            cur = self._client.cursor(cursor_factory=psycopg2.extras.DictCursor)
            cur.execute(query)
            res = cur.fetchall()
            data_block = DataSet(filter)
            data_block.data = res
            return data_block

        finally:
            if cur:
                cur.close()
