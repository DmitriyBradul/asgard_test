import pickle
import os.path
import string

from itertools import islice

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

from .data_provider import DataProvider



class SpreadsheetDataProvider(DataProvider):
    SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly']
    SPREADSHEET_ID = '1JFS9_Mhd538dr1NQh9ZFP1xarDxido7KM5-UNdRfCes'
    _client = None

    def __init__(self, uri_config):
        super().__init__(uri_config)
        #self._client = None
        self._sheets_metadata = []


    def connect(self):

        # If modifying these scopes, delete the file token.pickle.

        """Shows basic usage of the Sheets API.
        Prints values from a sample spreadsheet.
        """
        creds = None
        # The file token.pickle stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if os.path.exists('token.pickle'):
            with open('token.pickle', 'rb') as token:
                creds = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'price_engine/settings/credentials.json', self.SCOPES)
                creds = flow.run_local_server(port=0)
            # Save the credentials for the next run
            with open('token.pickle', 'wb') as token:
                pickle.dump(creds, token)

        service = build('sheets', 'v4', credentials=creds)

        # Call the Sheets API
        SpreadsheetDataProvider._client = service.spreadsheets()


    def _fetch_metadata(self):
        """
        Fetch metadata: sheet titles, columns, column types
        :return: None
        """

        MAX_ROWS = 100

        def _column_name(num):
            """
            Convert number into Excel-like column names, e.g. 1->A, 27-> AA, 55->BC
            :param num:
            :return:
            """
            UPPERS = string.ascii_uppercase
            div, mod = divmod(num, len(UPPERS))
            column_name = UPPERS[num-1] if div==0 else '%s%s' % (UPPERS[div-1], UPPERS[mod-1])
            return column_name

        def _type_guess(value):
            try:
                float(value)
                result = 'NUMBER'
            except Exception:
                if value.lower() in ('true', 'false'):
                    result = 'BOOLEAN'
                else:
                    result = 'STRING'
            return result

        sheet = self._client.get(spreadsheetId=self.SPREADSHEET_ID).execute()
        sheets = sheet.get('sheets', '')

        sheet_titles = {
            sheet.get('properties', {}).get("title", "Sheet1") : \
            sheet.get('properties', {}).get("gridProperties", {})
            for sheet in sheets
        }

        for sheet_title, dims in sheet_titles.items():
            sheet_metadata = {}
            sheet_metadata['title'] = sheet_title
            num_columns = dims.get('columnCount')
            num_rows = dims.get('rowCount')

            sheet_range = '%s!A1:%s%d' % (sheet_title, _column_name(num_columns),
                                          min(MAX_ROWS, num_rows))
            data = self._client.values().get(
                spreadsheetId=self.SPREADSHEET_ID,
                range=sheet_range
            ).execute()
            values = data.get('values', [])
            headers = values[0]
            sheet_metadata['column_headers'] = headers
            sheet_metadata['column_types'] = ['']*num_columns #len(headers)
            try:
                for row in islice(values, 1, MAX_ROWS):
                    for i, cell in enumerate(row):
                        assert i < len(sheet_metadata['column_types']), 'headers: %s, row: %s' % (headers, row)
                        if cell and not sheet_metadata['column_types'][i]:
                            sheet_metadata['column_types'][i] = _type_guess(cell)

                    if all(x for x in sheet_metadata['column_types']):
                        break
            except Exception as ex:
                import traceback
                traceback.print_exc()
                pass

            self._sheets_metadata.append(sheet_metadata)


    def fetch_metadata(self, doc_id=SPREADSHEET_ID):
        """
        Fetch Google spreadsheet by ID
        :param doc_id:
        :return:
        """

        if not self._client:
            self.connect()
        self._fetch_metadata()

        return self._sheets_metadata

