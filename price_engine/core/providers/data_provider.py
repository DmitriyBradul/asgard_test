import json


class DataProvider(object):

    def __init__(self, uri_config):
        self._uri_config = uri_config


    def connect(self):
        """
        Connect to data source using URI config
        :return: None
        """
        pass

    def fetch_metadata(self):
        """
        Fetch metadata using 'uri' field in uri config
        :return: metadata
        """
        pass

    def fetch_data(self, filter):
        """
        Fetch data using 'uri' field in uri config
        :return: data
        """
        pass
