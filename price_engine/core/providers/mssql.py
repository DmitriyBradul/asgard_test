from .data_provider import DataProvider


class MSSqlDataProvider(DataProvider):
    _client = None

    def __init__(self, uri_config):
        super().__init__(uri_config)
        self._metadata = []
