from PyQt5 import QtGui
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QStandardItem

from price_engine.core.entities.revision import Revision


class RevisionModel(QtGui.QStandardItemModel):

    def __init__(self, *args):
        super().__init__(*args)

    def appendRow(self, entry):
        # super().insertRow(self.rowCount())
        # model_index = self.index(self.rowCount()-1, 0)
        #
        # self.setData(model_index, entry, Qt.UserRole)

        item = QStandardItem(entry.name)
        item.setData(entry, Qt.UserRole)

        return super().appendRow([
            item,
            QStandardItem(entry.timestamp),
            QStandardItem(entry.hash),
        ])

    def headerData(self, p_int, Qt_Orientation, role=None):
        if Qt_Orientation == Qt.Horizontal and role==Qt.DisplayRole:
            return Revision.ATTRIBUTES[p_int]

        return super().headerData(p_int, Qt_Orientation, role)

    def itemData(self, QModelIndex):
        return super().itemData(QModelIndex).get(Qt.UserRole)

    def setData(self, QModelIndex, entry, role):
        super().setData(QModelIndex, entry, role)

        item = QStandardItem(entry.name)
        item.setData(entry, Qt.UserRole)

        super().setItem(QModelIndex.row(), 0, item)
        super().setItem(QModelIndex.row(), 1, QStandardItem(entry.timestamp))
        super().setItem(QModelIndex.row(), 2, QStandardItem(entry.hash))
