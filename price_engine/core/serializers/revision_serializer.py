import pickle

from price_engine.core.serializers.widget_serializer import TableWidgetSerializer, ListWidgetSerializer

class ModelSerializer:
    widgets = []
    serializers = []

    @staticmethod
    def pack(serializer, obj, values):
        return (pickle.dumps(serializer, protocol=0).decode(),
                pickle.dumps(obj, protocol=0).decode(),
                values)

    @staticmethod
    def unpack(data):
        bootstrapper, obj, values = data
        serializer = pickle.loads(bootstrapper.encode())
        obj = pickle.loads(obj.encode())
        return serializer, obj, values

    def serialize(self, obj, context):
        values = []
        for i, widget in enumerate(self.widgets):
            serializer = self.serializers[i]
            values.append(serializer.serialize(getattr(context, widget)))
        return ModelSerializer.pack(self, obj, values)

    def deserialize(self, context, values):
        for i, (serialized, widget) in enumerate(zip(values, self.widgets)):
            serializer = self.serializers[i]
            serializer.deserialize(serialized, getattr(context, widget))


class RuleRevisionSerializer(ModelSerializer):

    widgets = [
        'columnsList'
    ]

    serializers = [
        ListWidgetSerializer
    ]


class DataRevisionSerializer(ModelSerializer):

    widgets = [
        'dataTable'
    ]

    serializers = [
        TableWidgetSerializer
    ]


class RuleDataRevisionSerializer(ModelSerializer):

    widgets = RuleRevisionSerializer.widgets + \
              DataRevisionSerializer.widgets

    serializers = RuleRevisionSerializer.serializers + \
                  DataRevisionSerializer.serializers
