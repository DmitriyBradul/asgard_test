import json
import pickle

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QListWidgetItem


class ListWidgetSerializer:

    @staticmethod
    def serialize(source):
        items = [source.item(i).data(Qt.UserRole)
                 for i in range(source.count())]

        result = [pickle.dumps(rule, protocol=0).decode()
                   for rule in items]

        return result

    @staticmethod
    def deserialize(dumped, dest):
        dest.clear()
        for value in dumped:
            rule = pickle.loads(value.encode())
            item = QListWidgetItem(str(rule), dest)
            item.setData(Qt.UserRole, rule)


class TableWidgetSerializer:

    @staticmethod
    def serialize(source):

        data = []
        for i in range(source.rowCount()):
            row = []
            for j in range(source.columnCount()):
                curr_cell = source.item(i, j)
                row.append((curr_cell.text(), curr_cell.data(Qt.UserRole)))
            data.append(row)

        result = [pickle.dumps(data_entry, protocol=0).decode()
                   for data_entry in data]

        return result

    @staticmethod
    def deserialize(dumped, dest):
        dest.clear()
        dest.setRowCount(0)
        dest.setColumnCount(0)

        _1st_cell = pickle.loads(dumped[0].encode())
        columns_num = len(_1st_cell)
        context_column_idx = columns_num + 1

        dest.setColumnCount(columns_num)
        # self._window.dataTable.setHorizontalHeaderLabels(columns_names + ['_context'])
        dest.setColumnHidden(context_column_idx, True)

        for i, value in enumerate(dumped):
            cells = pickle.loads(value.encode())
            rowPosition = dest.rowCount()
            dest.insertRow(rowPosition)
            for j, cell in enumerate(cells[:-1]):
                dest.setItem(i, j, QtWidgets.QTableWidgetItem('%s' % cell[0]))

            # set context
            context_item = QtWidgets.QTableWidgetItem()
            context_item.setData(Qt.UserRole, cells[-1])
            context_item.setText(str(cells[-1]))
            dest.setItem(i, context_column_idx, context_item)
