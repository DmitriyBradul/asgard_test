import re

from price_engine.settings import config

class DataEntry:
    """
    Base class for evaluated data values
    """
    def __init__(self, name, expression=None):
        self.name = name
        self.expression = expression
        self._context = {}

    def __str__(self):
        return self.name

    @property
    def expression(self):
        return self._expression

    @expression.setter
    def expression(self, value):
        self._expression = value
        self.eval_expression = self._expression

    @property
    def eval_expression(self):
        return self._eval_expression

    @eval_expression.setter
    def eval_expression(self, value):
        if value:
            for match in re.findall('(\[\S+::\S+::\S+\])+', value):
                value = value.replace(match, match.replace('::', '__')\
                                                  .replace('[',  '_') \
                                                  .replace(']',  '_'))
        self._eval_expression = value

    @property
    def context(self):
        return self._context

    @context.setter
    def context(self, value):
        self._context = value

    def value(self):
        pass


class DataField(DataEntry):
    """
    Data entry from data providers
    """
    def __init__(self, data_source, table, column, type=None):
        super().__init__('')
        self.data_source = data_source
        self.table = table
        self.column = column
        self.type = type
        self.name = self.fully_qualified()
        self.expression = '[%s]' % self.name
        # self.eval_expression = self.expression

    def __str__(self):
        return self.fully_qualified()

    def __iter__(self):
        return iter((
            self.data_source,
            self.table,
            self.column,
        ))

    def fully_qualified(self):
        return '%s::%s::%s' % (self.data_source,
                               self.table,
                               self.column)

    def value(self):
        return eval(self.eval_expression, self._context)


class DataExpression(DataEntry):
    """
    Calculated Excel-like expression
    """
    def __init__(self, name, expression=None):
        super().__init__(name, expression)
        self._link = None

    def value(self):
        if self._link:
            self._context.update({
                self._link.name: self._link.value()
            })
        return eval(self.eval_expression, self._context)


class DataFilter:
    """
    Data filter
    """

    def __init__(self, name, table='(default)'):
        self.name = name
        self.table = table
        self._columns = []

    def add_column(self, column):
        self._columns.append(column)

    @property
    def columns(self):
        return self._columns

    @columns.setter
    def columns(self, columns):
        self._columns = columns.copy()


class DataSet:
    """
    Data set
    """

    def __init__(self, filter=None):
        self.filter = filter
        self.data = None
