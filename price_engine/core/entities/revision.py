import datetime

from hashlib import blake2b

hasher = blake2b(digest_size=12)

class Revision:
    """
    Revision tags rules, data and union of rules and data
    """

    ATTRIBUTES = ['name', 'timestamp', 'hash']

    def __init__(self, name='', tag=''):
        self._name = name
        self._tag = tag
        self.timestamp = str(datetime.datetime.now())#.replace(microsecond=0))
        hasher.update(str(self).encode())
        self.hash = hasher.hexdigest()

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value
        hasher.update(str(self).encode())
        self.hash = hasher.hexdigest()

    def __str__(self):
        return '%s (%s) %s' % (self._name, self._tag, self.timestamp)
