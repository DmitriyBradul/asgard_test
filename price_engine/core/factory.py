from price_engine.core.adapters.data_adapter import PostgresqlDataAdapter, SpreadsheetDataAdapter
from price_engine.core.providers import PosgresqlDataProvider, SpreadsheetDataProvider


class AbstractFactory:

    @staticmethod
    def get_factory(type_str):
        factory = {
            'spreadsheets' : SpreadsheetFactory,
            'postgresql' : PosgreSqlFactory,
            'mssql': MSSqlFactory
        }.get(type_str)

        if factory is None:
            raise ValueError('Type is not supported: %s' % type)

        return factory


class PosgreSqlFactory:

    fetcher = PosgresqlDataProvider
    adapter = PostgresqlDataAdapter


class SpreadsheetFactory:

    fetcher = SpreadsheetDataProvider
    adapter = SpreadsheetDataAdapter


class MSSqlFactory:

    fetcher = None
    adapter = None
