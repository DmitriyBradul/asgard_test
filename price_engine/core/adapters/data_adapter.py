from PyQt5 import QtGui

class DataAdapter:
    """
    Transforms data to format expected by UI-adapted.
    This allows to decouple UI from business-logic
    """
    DIMENSION_TYPES = {
        'STRING': './resources/icons/type-string.png',
        'BOOLEAN': './resources/icons/type-boolean.png',
        'DATETIME': './resources/icons/type-datetime.png'
    }
    MEASURE_TYPES = {
        'NUMBER': './resources/icons/type-number.png',
    }
    METADATA_SCHEMA = [
        {
            'title': str,
            'column_headers': list,
            'column_types': list
        }
    ]

    def _match(self) -> bool:
        pass


class PostgresqlDataAdapter(DataAdapter):

    def apply_metadata_schema(self, metadata):
        TYPES_CONVERT = {
            'varchar': 'STRING',
            'timestamp': 'DATETIME',
            'float8': 'NUMBER',
            'int4': 'NUMBER',
            'bool': 'BOOLEAN'
        }

        tag, field_names, field_types = metadata

        ui_field_types = [TYPES_CONVERT.get(type, type) for type in field_types]

        ui_metadata = [
            {
                'title': tag,
                'column_headers': field_names,
                'column_types': ui_field_types
            }
        ]

        return ui_metadata


class SpreadsheetDataAdapter(DataAdapter):

    def apply_metadata_schema(self, metadata):
        return metadata




