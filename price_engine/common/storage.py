from pymongo import MongoClient


_client = MongoClient('192.168.88.248', 27017)

_db = _client.main


def set(group, value):
    collection = _db[group]
    inserted_id = collection.insert_one(value).inserted_id
    return inserted_id


def get(group, filter):
    collection = _db[group]
    it = collection.find(filter)
    return it


def remove(group, filter):
    collection = _db[group]
    result = collection.delete_one(filter)
    return result.deleted_count


def update(group, filter, new_object, upsert=True):
    collection = _db[group]
    result = collection.update_one(
        filter=filter,
        update={'$set': new_object},
        upsert=upsert
    )
    return result.modified_count