#?FROM alpine
FROM ubuntu:bionic

RUN mkdir -p ./Downloads
WORKDIR ./Downloads

RUN apt-get update
RUN apt-get install -y wget gnupg software-properties-common apt-utils

RUN echo "------ Add python ------"
RUN wget https://www.python.org/ftp/python/3.7.6/python-3.7.6-amd64.exe

RUN echo "------ Add latest wine repo ------"
RUN dpkg --add-architecture i386
RUN wget -nc https://dl.winehq.org/wine-builds/winehq.key
RUN apt-key add winehq.key
RUN apt-add-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ bionic main'
RUN apt-get update

RUN echo "------ Add repo for faudio package.  Required for winedev ------"
RUN add-apt-repository -y ppa:cybermax-dexter/sdl2-backport
RUN wget https://download.opensuse.org/repositories/Emulators:/Wine:/Debian/xUbuntu_18.04/Release.key
RUN apt-key add Release.key
RUN apt-add-repository 'deb https://download.opensuse.org/repositories/Emulators:/Wine:/Debian/xUbuntu_18.04/ ./'
RUN apt-get update

RUN echo "------ Install wine-devel ------"
RUN apt-get install -y wine-devel winehq-devel winetricks xvfb

RUN echo "------ Add user ------"
RUN adduser --disabled-password --gecos "" wine_user --quiet
RUN su - wine_user

RUN echo "------ Init wine prefix ------"
RUN WINEPREFIX=~/.wine64 WINARCH=win64 winetricks \
    corefonts \
    win10

#RUN pip install -r /requirements.txt

RUN echo "------ Setup dummy screen ------"
RUN Xvfb :0 -screen 0 1024x768x16 &

RUN echo "------ Install python ------"
RUN apt-get install -y vim
#RUN export DISPLAY=:0
#RUN DISPLAY=:0.0 WINEPREFIX=~/.wine64 wine cmd /c \
RUN DISPLAY=:0 WINEPREFIX=~/.wine64 wine cmd /c \
    python-3.7.6-amd64.exe \
    /quiet \
    PrependPath=1 \
    && echo "Python Installation complete!"


RUN echo "------ Install dependencies ------"
RUN PYTHON_DIR=~/.wine64/drive_c/users/wine_user/Local\ Settings/Application\ Data/Programs/Python/Python37/
COPY ./requirements.txt /requirements.txt
RUN WINEPREFIX=~/.wine64 wine "$PYTHON_DIR"/python.exe -m pip install -r /requirements.txt

# RUN echo "------ Build ------"
# RUN WINEPREFIX=~/.wine64 wine "$PYTHON_DIR"/Scripts/pyinstaller.exe -y main.py

# RUN echo "------ Deploy ------"
# RUN zip -r  dist/main.zip dist/main
