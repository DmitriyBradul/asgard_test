#!/bin/bash
set -v

ROOT_DIR=/home/dbhost/Work/qt_test/price_engine
pyuic5 $ROOT_DIR/price_engine/views/source_code_edit.ui -o $ROOT_DIR/price_engine/views/source_code_edit.py
pyuic5 $ROOT_DIR/price_engine/views/mainwindow.ui -o $ROOT_DIR/price_engine/views/mainwindow.py
pyuic5 $ROOT_DIR/price_engine/views/preferences.ui -o $ROOT_DIR/price_engine/views/preferences.py
pyuic5 $ROOT_DIR/price_engine/views/preferences_mongodb.ui -o $ROOT_DIR/price_engine/views/preferences_mongodb.py
pyuic5 $ROOT_DIR/price_engine/views/preferences_appearance.ui -o $ROOT_DIR/price_engine/views/preferences_appearance.py
pyuic5 $ROOT_DIR/price_engine/views/preferences_general.ui -o $ROOT_DIR/price_engine/views/preferences_general.py
pyuic5 $ROOT_DIR/price_engine/views/preferences_datasources.ui -o $ROOT_DIR/price_engine/views/preferences_datasources.py

