import sys
import traceback

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QStyleFactory

from price_engine.views.mainwindow import Ui_MainWindow
from price_engine.views.mainwindow_actions import Ui_MainWindow_Actions
from price_engine.views.mainwindow_custom import Ui_MainWindow_Custom
from price_engine.settings import config

def excepthook(exc_type, exc_value, exc_tb):
    tb = "".join(traceback.format_exception(exc_type, exc_value, exc_tb))
    print("error catched!:")
    print("error message:\n", tb)
    error_dialog = QtWidgets.QErrorMessage()
    error_dialog.showMessage("Error message:\n %s" % tb)
    _ = error_dialog.exec_()

def setup_settings():
    QtWidgets.QApplication.setOrganizationName("USWF")
    QtWidgets.QApplication.setOrganizationDomain("https://www.commercialwaterdistributinginc.com/")
    QtWidgets.QApplication.setApplicationName("PriceEngine")

if __name__ == "__main__":
    sys.excepthook = excepthook
    setup_settings()
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    ui._mainwindow = MainWindow
    config.app = app

    ui_actions = Ui_MainWindow_Actions(ui)
    ui_actions.connect_handlers()

    ui_custom = Ui_MainWindow_Custom()
    ui_custom.setupUi(ui, ui_actions)
    ui_custom.on_startup()

    MainWindow.showMaximized()
    sys.exit(app.exec_())

