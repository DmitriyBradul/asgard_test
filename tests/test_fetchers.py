from unittest import TestCase

from price_engine.core.providers import  SpreadsheetDataProvider, PosgresqlDataProvider
from price_engine.settings import config


class GoogleFetcherTests(TestCase):
    """
    Test data fetchers
    """

    DATA_SOURCE_NAME = 'Google_Sheet'

    # def setUp(self):
    #     self.client = APIClient()


    def test_google_spreadsheet_fetch(self):
        """
        Test creating using with a valid payload is successful
        """
        uri = config.get(self.DATA_SOURCE_NAME)
        res = SpreadsheetDataProvider(uri).fetch_metadata()
        self.assertIsNotNone(res)
        self.assertTrue(len(res)>0)



class PostgresqlFetcherTests(TestCase):
    """
    Test data fetchers
    """

    DATA_SOURCE_NAME = 'Mapper'

    # def setUp(self):
    #     self.client = APIClient()


    def test_connection(self):
        """
        Test postgres connection
        """

        uri = config.get(self.DATA_SOURCE_NAME)
        res = PosgresqlDataProvider(uri).connect()
        self.assertTrue(True)


    def test_fetch_metadata(self):
        """
        Test postgres metadata fetch
        """

        # uri = config.get(self.DATA_SOURCE_NAME)
        uri = config.get(self.DATA_SOURCE_NAME)
        res = PosgresqlDataProvider(uri).fetch_metadata()
        self.assertIsNotNone(res)
        self.assertTrue(len(res) > 0)


    def test_fetch_data(self):
        """
        Test postgres data fetch
        """

        uri = config.get(self.DATA_SOURCE_NAME)
        posgres_fetcher = PosgresqlDataProvider(uri)
        res = posgres_fetcher.fetch_data()
        self.assertIsNotNone(res)
        self.assertTrue(len(res)>0)
