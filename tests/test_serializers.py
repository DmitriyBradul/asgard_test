import PyQt5

from unittest import TestCase
from unittest import mock
from unittest import mock

from price_engine.common import storage
from price_engine.core.serializers import RuleRevisionSerializer
from price_engine.settings import config
from PyQt5 import QtWidgets



class RuleSerializerTestCase(TestCase):

    #@mock.patch('PyQt5.QtWidgets.QListWidget')
    @mock.patch.object(PyQt5.QtWidgets, 'QListWidget', autospec=True)
    def test_serialize(self, mock_obj):
        # set up the mock
        _ = mock_obj

        obj = 'test42'
        serializer = RuleRevisionSerializer()
        # TODO: mock UI context in serialize method
        list_widget = mock.create_autospec(QtWidgets.QListWidget)
        #res = serializer.serialize(obj, QtWidgets.QListWidgetItem('UI_test42'))
